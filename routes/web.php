<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('about', 'AboutController@index');

// contoh invokable
Route::get('nested', 'NestedController');

Route::get('search', 'SearchController@post')->name('search-post');

Route::middleware('auth')->prefix('post/')->group(function () { // menggunakan prefix route untuk mengatasu pengulangan penulisan 
    Route::get('', 'PostController@index')->name('posts-index')->withoutMiddleware('auth');
    Route::get('create', 'PostController@create')->name('post-create');
    Route::post('store', 'PostController@store')->name('post-store');
    Route::get('{post:slug}/edit', 'PostController@edit')->name('post-edit');
    Route::patch('{post:slug}/edit', 'PostController@update')->name('post-update');
    Route::delete('{post:slug}/delete', 'PostController@destroy')->name('post-delete');
    Route::get('{post:slug}', 'PostController@show')->name('post-show')->withoutMiddleware('auth'); // menggunakan Route key name
});
// Route::get('post/{slug}', 'PostController@show');

Route::get('categories/{category:slug}', 'CategoryController@show');

Route::get('tag/{tag:slug}', 'TagController@show'); // menggunakan Route key name



// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/login', function () {
//     return view('login');
// });

// Route::get('/about', function() {
//     $parsing_data = 'Tedy Hidayat';

//     return view('about', ['view_data' => $parsing_data]);
// });

// Route::get('/nested', function() {
//     $parsing_data = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
//     Necessitatibus adipisci architecto libero facilis autem tempore quisquam 
//     voluptates deleniti odio fugiat laborum facere officia, ab ut. 
//     Laudantium expedita consectetur voluptatum veniam!';

//     return view('nested_folder.index', ['view_data' => $parsing_data]);
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
