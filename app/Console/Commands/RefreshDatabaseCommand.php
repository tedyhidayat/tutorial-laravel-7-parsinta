<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RefreshDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Artisan unutk refresh database dan membuat seeder default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $this->call('migrate:refresh');  // memanggil command database refresh
        $this->call('db:seed');  // memanggil command db:seed

        // bisa membuat langsung seeder di dalam command
        // $categories = collect(['Framework', 'Programming', 'Code']);
        // $categories->each(function($c){
        //     \App\Category::create([
        //         'name' => $c,
        //         'slug' => \Str::slug($c),
        //     ]);
        // });

        // $tags = collect(['Bugs', 'Help', 'Forum']);
        // $tags->each(function($c){
        //     \App\Tag::create([
        //         'name' => $c,
        //         'slug' => \Str::slug($c),
        //     ]);
        // });

        $this->info('Databsae telah direfresh dan seeder telah dibuat!');
    }
}
