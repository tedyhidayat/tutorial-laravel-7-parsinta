<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //protected $table = 'tb_posts'; // digubnakan jika tidak menggunakan aturan database dari laravel

    protected $fillable = ['title', 'slug', 'body', 'category_id', 'user_id', 'thumbnail']; //lebih aman
    // protected $guarded = []; // kurang aman, bisa diretas via form

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function takeImage()
    {
        return asset("storage/" . $this->thumbnail);
    }
    
    public function getTakeImageAttribute()
    {
        return asset("storage/" . $this->thumbnail);
    }
}
