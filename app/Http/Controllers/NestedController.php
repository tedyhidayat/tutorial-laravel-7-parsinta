<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NestedController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $name = '<h2>Tedy Hidayat</h2>';
        // $name = $request->name;
        $name = request('name');
        return view('nested_folder.index', compact('name'));
    }
}
