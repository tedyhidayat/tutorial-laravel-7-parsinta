<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request; 

use App\{Category, Post, Tag};
use App\Http\Requests\PostRequest;


class PostController extends Controller
{

    
    // public function __construct()
    // {
    //     $this->middleware('auth')->except([
    //         'index', 'show'
    //     ]);
    // }
    

    public function index()
    {
        // $posts = Post::get();
        $posts = Post::with([
            'tags', 'category', 'author'
        ])->orderBy('id', 'desc')->paginate('6');
        // $posts = Post::simplePaginate('2');
        // $posts = Post::take(3)->get();
        // $posts = Post::orderBy('id', 'DESC')->get();
        return view('post.index', compact('posts'));
    }

    public function show(Post $post)
    {
        $posts = Post::with(['tags', 'category', 'author'])->where('category_id', $post->category_id)->latest()->limit(3)->get();
        // return view('post.show', compact('post', 'posts'));
        return view('post.show', ['post' => $post, 'posts' => $posts]);
    }

    public function create()
    {
        // form input partial  ['post' => new post()]
        return view('post.create', [
            'post' => new post(), // untuk mendeklarasikan post baru di form partial
            'submit' => 'Create', // untuk menamakan button submit di from partial
            'tags' => Tag::get(), // ambil dari model tag
            'categories' => Category::get(), // ambil dari model category
        ]);
    }

    public function store(PostRequest $request)
    {

        // validation
        // $this->validateRequest();

        // cara 1
        // $post = new Post;
        // $post->title = $request->title;
        // $post->slug = \Str::slug($request->title);
        // $post->body = $request->body;
        // $post->save();

        // cara 2
        // Post::create([
        //     'title' => $request->title,
        //     'slug' => \Str::slug($request->title),
        //     'body' => $request->body,
        // ]);

        // cara 3
        $request->validate([
            'thumbnail' => 'image|mimes:jpg,png,jpeg,svg|max:2048'
        ]);

        $attr = $request->all();

        $slug = \Str::slug($request->title);
        $attr['slug'] = $slug;

        $thumbnail = request()->file('thumbnail');

        if($thumbnail) {
            $thumbnailUrl = $thumbnail->storeAs("images/posts", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbnailUrl = null;
        }


        $attr['thumbnail'] = $thumbnailUrl;
        $attr['category_id'] = request('category');
        $attr['user_id'] = auth()->id();
        
        $post = Post::create($attr);

        $post->tags()->attach(request('tags')); // untuk mengambil array dari input array

        // flash daata
        session()->flash('success', 'The post was created');
        // session()->flash('error', 'Failed creating post!');

        // return redirect('post');
        // return back();
        return redirect('post');

    }

    public function edit(Post $post)
    {
        $this->authorize('update', $post); // memanggil policy

        return view('post.edit', [
            'post' => $post, // untuk mendeklarasikan post baru di form partial
            'tags' => Tag::get(), // ambil dari model tag
            'categories' => Category::get(), // ambil dari model category
        ]);
    }

    public function update(PostRequest $request, Post $post)
    {
        // dd($post); 
         // validation

        $this->authorize('update', $post); // memanggil policy

        $request->validate([
            'thumbnail' => 'image|mimes:jpg,png,jpeg,svg|max:2048'
        ]);
        
        $attr = $request->all();

        if(request()->file('thumbnail')) {
            \Storage::delete($post->thumbnail);
            $slug = \Str::slug($request->title);
            $thumbnail = request()->file('thumbnail');
            $thumbnailUrl = $thumbnail->storeAs("images/posts", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbnailUrl = $post->thumbnail;
        }
        
        
        $attr['category_id'] = request('category');
        $attr['thumbnail'] = $thumbnailUrl;
        $post->update($attr);

        $post->tags()->sync($request->tags);

        session()->flash('success', 'The post was updated');

        return redirect('post');
        
    }
    
    public function destroy(Post $post)
    {  
        // if(auth()->user()->id == $post->user_id)
        // {
        //     $post->tags()->detach();    // untuk menghapus tags
        //     $post->delete();
    
        //     session()->flash('success', 'The post was deleted.');
        // }
        // else 
        // {
        //     session()->flash('error', 'Sorry, you cannot delete this post');
        // }

        // return redirect('post');
        $this->authorize('delete', $post); // memanggil policy
        
        \Storage::delete($post->thumbnail); // untuk menghapus gambar di storage
        $post->tags()->detach();    // untuk menghapus tags
        $post->delete();

        session()->flash('success', 'The post was deleted');

        return redirect('post');
    }

    // public function validateRequest()
    // {
    //     return request()->validate([
    //         'title' => 'required|min:5',
    //         'body' => 'required',
    //     ]);
    // }
}


