<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $name = "<b>Tedy Hidayat</b>";
        // compact() pengganti array ['name'=>$name]
        return view('about', compact('name'));
    }
}
