@extends('layouts.app', ['title' => 'Create Post'])

{{-- @section('title', 'Create Post') --}}
    
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <div class="card">
                    <div class="card-header">
                        Create New Post
                    </div>
                    <div class="card-body">
                        <form action="/post/store" method="post" enctype="multipart/form-data">
                        @csrf
                            {{-- <div class="form-group">
                                <label for="title">Title Post</label>
                                <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror">
                                @error('title')
                                    <div class="text-danger text-small invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="body">Article</label>
                                <textarea type="text" name="body" id="body" class="form-control"></textarea>
                                @error('body')
                                    <div class="text-danger text-small">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button> --}}

                        @include('post.partials.form-control')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection