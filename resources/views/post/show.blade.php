@extends('layouts.app')

@section('title', $post->title)
    
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                @if($post->thumbnail)
                    {{-- <img src="{{ asset("storage/" . $post->thumbnail) }}" class="card-img-top"> --}}
                    {{-- <img src="{{ asset($post->takeImage()) }}" class="card-img-top">  --}}
                    <a href="{{ route('post-show', $post->slug) }}">
                        <img src="{{ $post->takeImage }}" class="card-img-top" style="object-fit:cover; object-position:center; height:500px;"> 
                    </a>
                @endif
                
                <h3 class="card-title mt-4"><a class="text-dark" href="{{ route('post-show', $post->slug) }}">{{ $post->title }}</a></h3>
                <div class="text-secondary">
                    {!! nl2br($post->body) !!}
                </div>
                {{-- <a href="{{ route('post-show', $post->slug) }}">Read More</a> --}}
                {{-- <a href="/post/{{ $post->slug }}">Read More</a> --}}
            

                <div class="mt-5">
                    <small class="text-info"> Category: {{ $post->category->name }}</small>
                    <br>
                    <small>
                        Tags: 
                        @foreach ($post->tags as $tag)
                            <a href="/tag/{{ $tag->slug }}">{{ $tag->name }}</a>,
                        @endforeach
                    </small>
                    <br>
                    <small class="text-muted">
                        Author : {{ $post->author->name }}
                    </small>
                    <br>
                    {{-- Published at {{ $post->created_at->format('d, F Y') }} --}}
                    <small class="text-muted"> Published at <b>{{ $post->created_at->diffForHumans() }}</b></small>
                </div>
            </div>
            <div class="col-md-5">
                @foreach ($posts as $post)
                <div class="card my-3 ">
                    <div class="card-body">
                        <h5 class="card-title"><a class="text-dark" href="{{ route('post-show', $post->slug) }}">{{ $post->title }}</a></h5>
                        <small class="text-info"> Category: {{ $post->category->name }}</small>
                        <br>
                        <small class="text-muted">
                            Author : {{ $post->author->name }}
                        </small>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection