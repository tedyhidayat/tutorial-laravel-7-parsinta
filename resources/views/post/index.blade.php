@extends('layouts.app')

@section('title', 'All Post')
    
@section('content')
    <div class="container">
        <div class="d-flex justify-content-between">
            <div>
                @isset($category)
                    <h1>Category: {{ $category->name }}</h1>
                @endisset

                @isset($tag)
                    <h1>Tag: {{ $tag->name }}</h1>
                @endisset
                
                @isset($query)
                    <h1>Keyword: {{ $query }}</h1>
                @endisset

                @if(!isset($category) && !isset($tag) && !isset($query))
                    <h1>All Post</h1>
                @endif
            </div>
            <div>
                @auth
                <a href="{{ route('post-create') }}" class="btn btn-success">Create Post</a>
                @endauth
                @guest
                <a href="{{ route('login') }}" class="btn btn-info">Login to create new post</a>
                @endguest
            </div>
        </div>
        
        <div class="row">
            @forelse ($posts as $post)
                <div class="col-md-7">
                    <div class="card my-3 ">
                        @if($post->thumbnail)
                            {{-- <img src="{{ asset("storage/" . $post->thumbnail) }}" class="card-img-top"> --}}
                            {{-- <img src="{{ asset($post->takeImage()) }}" class="card-img-top">  --}}
                            <a href="{{ route('post-show', $post->slug) }}">
                                <img src="{{ $post->takeImage }}" class="card-img-top" style="object-fit:cover; object-position:center; height:500px;"> 
                            </a>
                        @endif
                        <div class="card-body">
                            <h5 class="card-title"><a class="text-dark" href="{{ route('post-show', $post->slug) }}">{{ $post->title }}</a></h5>
                            <div class="text-secondary">
                                {{Str::limit($post->body, 200, '~') }}
                            </div>
                            {{-- <a href="{{ route('post-show', $post->slug) }}">Read More</a> --}}
                            {{-- <a href="/post/{{ $post->slug }}">Read More</a> --}}
                        </div>
                        <div class="card-body">
                            <small class="text-info"> Category: {{ $post->category->name }}</small>
                            <br>
                            <small>
                                Tags: 
                                @foreach ($post->tags as $tag)
                                    <a href="/tag/{{ $tag->slug }}">{{ $tag->name }}</a>,
                                @endforeach
                            </small>
                            <br>
                            <small class="text-muted">
                                Author : {{ $post->author->name }}
                            </small>
                            <br>
                            {{-- Published at {{ $post->created_at->format('d, F Y') }} --}}
                            <small class="text-muted"> Published at <b>{{ $post->created_at->diffForHumans() }}</b></small>
                        </div>
                        <div class="card-footer">
                            {{-- @if(auth()->user()->id == $post->user_id) --}}
                            @can('update', $post)
                                <a href="/post/{{ $post->slug }}/edit" class="btn btn-info btn-sm">Edit</a>
                            @endcan
                            @can('delete', $post)
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                Delete
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Yakin hapus data ?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $post->title }}
                                            <br>
                                            <small class="text-muted">
                                                Posted at: {{ $post->created_at->format('d, M Y') }}
                                            </small>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Tidak</button>
                                            <form action="/post/{{ $post->slug }}/delete" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger">Ya</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endcan
                            {{-- @endif --}}
                        </div>
                    </div>
                </div>

            @empty
                <div class="col-md-12">
                    <div class="alert alert-info">
                        Sorry data not exist.
                    </div>
                </div>
            @endforelse
        </div>
        <div class="d-flex justify-content-center">
            <div>
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection
