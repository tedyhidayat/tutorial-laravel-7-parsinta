@extends('layouts.app', ['title' => 'Update Post'])

{{-- @section('title', 'Create Post') --}}
    
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <div class="card">
                    <div class="card-header">
                        Update post: <b>{{ $post->title }}</b>
                    </div>
                    <div class="card-body">
                        <form action="/post/{{ $post->slug }}/edit" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        {{-- form ada di folder partials --}}
                        @include('post.partials.form-control')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection