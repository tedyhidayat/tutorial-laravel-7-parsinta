<div class="form-group">
    <label for="thumbnail">Image</label>
    <input type="file" name="thumbnail" id="thumbnail" class="form-control @error('thumbnail') is-invalid @enderror">
    @error('thumbnail')
        <div class="t   ext-danger text-small invalid-feedback">
            {{ $message }}
        </div>    
    @enderror
</div>
<div class="form-group">
    <label for="title">Title Post</label>
    <input type="text" name="title" id="title" value="{{ old('title') ?? $post->title }}" class="form-control @error('title') is-invalid @enderror">
    @error('title')
        <div class="text-danger text-small invalid-feedback">
            {{ $message }}
        </div>    
    @enderror
</div>
<div class="form-group">
    <label for="category">category Post</label>
    <select type="text" name="category" id="category" class="form-control @error('title') is-invalid @enderror">
        <option disabled selected>Pilih kategori</option>
        @foreach ($categories as $category)
            <option {{ $post->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>
    @error('category')
        <div class="text-danger text-small invalid-feedback">
            {{ $message }}
        </div>    
    @enderror
</div>
<div class="form-group">
    <label for="tags">Tags</label>
    <select type="text" name="tags[]" id="tags" class="form-control selecttu @error('tags') is-invalid @enderror" multiple>
        @foreach ($post->tags as $tag)
            <option selected value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
        @foreach ($tags as $tag)
            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
    @error('tags')
        <div class="text-danger text-small invalid-feedback">
            {{ $message }}
        </div>    
    @enderror
</div>
<div class="form-group">
    <label for="body">Article</label>
    <textarea type="text" name="body" id="body" class="form-control">{{ old('body') ?? $post->body }}</textarea>
    @error('body')
        <div class="text-danger text-small">
            {{ $message }}
        </div>    
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>