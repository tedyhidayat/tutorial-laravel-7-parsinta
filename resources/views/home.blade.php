@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @auth   
                    Hello, {{ Auth::user()->name }}
                    @endauth
                    @guest
                        Welcome
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        @forelse ($posts as $post)
            <div class="col-md-4">
                <div class="card my-3 ">
                    @if($post->thumbnail)
                        {{-- <img src="{{ asset("storage/" . $post->thumbnail) }}" class="card-img-top"> --}}
                        {{-- <img src="{{ asset($post->takeImage()) }}" class="card-img-top">  --}}
                        <a href="{{ route('post-show', $post->slug) }}">
                            <img src="{{ $post->takeImage }}" class="card-img-top" style="object-fit:cover; object-position:center; height:300px;"> 
                        </a>
                    @endif
                    <div class="card-body">
                        <h5 class="card-title"><a class="text-dark" href="{{ route('post-show', $post->slug) }}">{{ $post->title }}</a></h5>
                        <div class="text-secondary">
                            {{Str::limit($post->body, 200, '~') }}
                        </div>
                        {{-- <a href="{{ route('post-show', $post->slug) }}">Read More</a> --}}
                        {{-- <a href="/post/{{ $post->slug }}">Read More</a> --}}
                    </div>
                    <div class="card-body">
                        <small class="text-info"> Category: {{ $post->category->name }}</small>
                        <br>
                        <small>
                            Tags: 
                            @foreach ($post->tags as $tag)
                                <a href="/tag/{{ $tag->slug }}">{{ $tag->name }}</a>,
                            @endforeach
                        </small>
                        <br>
                        <small class="text-muted">
                            Author : {{ $post->author->name }}
                        </small>
                        <br>
                        {{-- Published at {{ $post->created_at->format('d, F Y') }} --}}
                        <small class="text-muted"> Published at <b>{{ $post->created_at->diffForHumans() }}</b></small>
                    </div>
                </div>
            </div>

        @empty
            <div class="col-md-12">
                <div class="alert alert-info">
                    Sorry data not exist.
                </div>
            </div>
        @endforelse
    </div>
</div>
@endsection
