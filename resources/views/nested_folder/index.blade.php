@extends('layouts.app')

@section('title', 'nested')

@section('content')
<div class="container">
    <h1>Nested</h1>
    <p>
        {{-- {!! nl2br($view_data) !!}
        <br><br>
        {!! $view_data !!}
        <br><br>
        {{ $view_data }} --}}

        my name is {{ $name }}
        <br>
        my name is {!! $name !!}
    </p>
</div>
@endsection