<div class="container">
    <div class="row">
        <div class="col">

            @if(session()->has('success'))
            <div class="alert alert-success my-5">
                {{ session()->get('success') }}
            </div>
            @endif

            @if(session()->has('error'))
            <div class="alert alert-danger my-5">
                {{ session()->get('error') }}
            </div>
            @endif

        </div>
    </div>
</div>
