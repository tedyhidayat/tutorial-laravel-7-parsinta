<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Tedy Hidayat',
            'username' => 'tedyhidayat',
            'password' => bcrypt('123'),
            'email' => 'tedylara7@gmail.com',

        ]);
    }
}
